package module2task.dao;


import module2task.model.User;

public interface UserDao {
    User getUserByEmail(String email);
    User createUser(User user);
    User updateUser(User user);
    boolean deleteUser(int id);
}
