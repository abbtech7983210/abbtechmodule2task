package module2task.dao;

import module2task.model.Task;

import java.util.List;

public interface TaskDao {
    List<Task> getAllTasks();
    Task getTaskById(int id);
    Task createTask(Task task);
    Task updateTask(Task task);
    boolean deleteTask(int id);
}
