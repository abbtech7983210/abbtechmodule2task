package module2task.application;

import module2task.AppStarter;

public class Main {
    public static void main(String[] args) {
        AppStarter.init();

        System.out.println("Application started successfully.");

        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
