package module2task.controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import module2task.dao.UserDao;
import module2task.dao.UserService;
import module2task.model.User;
import org.mindrot.jbcrypt.BCrypt;
import java.io.IOException;

@WebServlet("/user")
public class UserServlet extends HttpServlet {
    private UserDao userDao;

    public void init() {
        userDao = new UserService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action != null) {
            switch (action) {
                case "login":
                    loginUser(request, response);
                    break;
                case "register":
                    registerUser(request, response);
                    break;
                default:
                    response.sendRedirect("/login.html");
            }
        } else {
            response.sendRedirect("/login.html");
        }
    }

    private void loginUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = userDao.getUserByEmail(email);
        if (user != null && user.getPassword().equals(password)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            response.sendRedirect("/tasks");
        } else {
            response.sendRedirect("/login.html?error=true");
        }
    }

    private void registerUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (username == null || username.trim().isEmpty() || email == null || email.trim().isEmpty() || password == null || password.isEmpty()) {
            response.sendRedirect("/register.html?error=true");
            return;
        }

        if (userDao.getUserByEmail(email) != null) {
            response.sendRedirect("/register.html?error=email_exists");
            return;
        }

        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());

        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(hashedPassword);

        User createdUser = userDao.createUser(user);
        if (createdUser != null) {
            HttpSession session = request.getSession();
            session.setAttribute("user", createdUser);
            response.sendRedirect("/tasks");
        } else {
            response.sendRedirect("/register.html?error=registration_failed");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Logout
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        response.sendRedirect("/login.html");
    }
}
