package module2task.controller;


import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import module2task.dao.TaskDao;
import module2task.dao.TaskService;
import module2task.model.Task;

import java.io.IOException;
import java.util.List;

@WebServlet("/tasks")
public class TaskServlet extends HttpServlet {
    private TaskDao taskDao;

    public void init() {
        taskDao = new TaskService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Task> tasks = taskDao.getAllTasks();
        request.setAttribute("tasks", tasks);
        request.getRequestDispatcher("/tasks.jsp").forward(request, response); // Forward to tasks.jsp page to display tasks
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String description = request.getParameter("description");

        Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);

        task = taskDao.createTask(task);
        response.sendRedirect("/tasks");
    }
}