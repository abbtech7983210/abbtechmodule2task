package module2task;

import jakarta.servlet.ServletContainerInitializer;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.HandlesTypes;
import module2task.controller.TaskServlet;
import module2task.controller.UserServlet;

import java.util.Set;

@HandlesTypes({javax.servlet.Servlet.class})
public class AppStarter implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        init();
        for (Class<?> servletClass : c) {
            if (servletClass.equals(UserServlet.class)) {
                ctx.addServlet("AuthServlet", UserServlet.class).addMapping("/user/*");
            } else if (servletClass.equals(TaskServlet.class)) {
                ctx.addServlet("TaskServlet", TaskServlet.class).addMapping("/tasks/*");
            }
        }
    }

    public static void init() {
        System.out.println("Initializing application...");
    }
}
